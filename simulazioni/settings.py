from os.path import dirname, abspath, join

BASE_DIR = dirname(abspath(__file__))

database_url = f"sqlite:///{BASE_DIR}/plots.sqlite3"
plot_dir = join(BASE_DIR, "plots")
