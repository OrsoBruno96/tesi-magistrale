#!/usr/bin/env python3
from setuptools import Extension, setup
from Cython.Build import cythonize

# Attenzione, attenzione. Il supporto per la libreria per la libc
# <complex.h> non è ancora distribuito con cython, per cui ho recuperato il sorgente da GitHub
# e ne ho fatto il monkeypatch

ext_modules = cythonize(module_list=[
    Extension("funzioni_agnostico",
              sources=["dolinar/funzioni_agnostico.pyx", ],
              libraries=["m"],
              # language="c++",
    ),
], annotate=True)


setup(name="dolinar",
      ext_modules=ext_modules,
)
