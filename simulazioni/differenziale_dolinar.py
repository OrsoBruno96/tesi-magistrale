#!/usr/bin/env python3
from numpy import arctan as atan, sqrt, abs, cos, sin, linspace, exp, pi, \
    isclose
from matplotlib import pyplot as plt


def theta_from_P(P, n):
    if isclose(P, .5):
        return pi/4
    return abs(.5*atan(sqrt(n)/(n - 1)*1/(P - .5)))


def mu(alpha, n, P):
    def theta(P):
        return theta_from_P(P, n)
    return alpha**2*(cos(theta(P)) + sqrt(n)*sin(theta(P)))**2


def lambda_(alpha, n, P):
    def theta(P):
        return theta_from_P(P, n)
    return alpha**2*(cos(theta(P)) - sqrt(n)*sin(theta(P)))**2


def increment(alpha, n, P):
    return mu(alpha, n, P) - (lambda_(alpha, n, P) + mu(alpha, n, P))*P


def simulate(alpha, n, stop):
    size = 10000
    x = linspace(0, stop, size)
    y = linspace(.5, .5, size)
    hel = helstrom(alpha*sqrt(stop))
    z = linspace(hel, hel, size)
    inc = stop/size
    for i in range(1, size):
        y[i] = y[i - 1] + inc*increment(alpha, n, y[i - 1])
    return x, y, z


def helstrom(alpha):
    return .5*(1 + sqrt(1 - exp(-4*alpha**2)))


def plot_for_n(n, plot_helstrom=False):
    x, y, z = simulate(.25, n, 2)
    theta = [theta_from_P(yy, n) for yy in y]
    plt.subplot(2, 1, 1)
    if plot_helstrom:
        plt.plot(x, z, linestyle='--', marker='', color='red', label='Helstrom bound')
    plt.plot(x, y, linestyle='-', marker='', label=f'$n = {n}$')
    plt.subplot(2, 1, 2)
    plt.plot(x, theta, linestyle='-', marker='', label=f'$n = {n}$')


def bellurie(subplot):
    plt.subplot(*subplot)
    plt.minorticks_on()
    plt.grid('gray')
    # plt.ylim(0, 1)
    plt.xlim(0, 2)
    plt.legend()


if __name__ == "__main__":
    plt.figure(1, figsize=(10, 9))
    plot_for_n(10, plot_helstrom=True)
    for n in [20, 30, 100, 500, 1000, 100000]:
        plot_for_n(n)
    bellurie((2, 1, 1))
    bellurie((2, 1, 2))
    plt.subplot(2, 1, 1)
    plt.yscale('log')
    plt.show()
