#!/usr/bin/env julia

using Random;
using LinearAlgebra;
import PyPlot;
using Printf;

"""Simulazione del percettrone proposto da Fanizza.
All'inizio creo dei dati random che sono dei punti in R^n divisi in due classi ben distinte.
Poi provo l'algorimo di update spiegato nel logbook e alla fine provo a plottare il
risultato piangendo lacrime amare.
"""


#####################
# Parametri globali #
#####################

# Dimensione di R^n
const n = 2;
# Totale di punti di training generati (e in questo caso anche di passi dell'update)
const N = 10000;

# Le seguenti variabili sono definite sul logbook e indicano quanto pezzo viene
# usato per il training e quanto conservato
const θ_1 = 0.1;
const θ_2 = 0.1;
const α = 0.1;
const centro1 = -5.;
const σ1 = 1;
const centro2 = 5.;
const σ2 = 1;


#########################
# end Parametri globali #
#########################


include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    error_probability, helstrom_success, log_approx_helstrom_error


"""Crea dei dati che stanno distribuiti in due gaussiane distinte.
La posizione dei centri è (centro1, centro1, ..., centro1) e
(centro2, centro2, ... centro2) e le distribuzioni hanno σ uniforme in tutte
le direzioni.
"""
function generate_data()
    training_data = Array{Float64, 2}(undef, n, N);
    training_labels = Array{Bool, 1}(undef, N);
    for i in 1:N
        training_labels[i] = rand(rng, Bool);
        training_data[:, i] = randn!(rng, zeros(n))
        @debug "Dati di training: " training_labels[i] training_data[:, i]
        if training_labels[i]
            training_data[:, i] .*= σ1
            training_data[:, i] .+= centro1
        else
            training_data[:, i] .*= σ2
            training_data[:, i] .+= centro2
        end
    end
    return training_data, training_labels
end


function get_mixed_components(data::Array{Float64, 1}, weights::Array{Float64, 1})
    x1 = (sin(θ_1).*data + sin(θ_2).*weights)/sqrt(2.)
    x2 = (sin(θ_1).*data - sin(θ_2).*weights)/sqrt(2.)
    return norm(x1)^2, norm(x2)^2
end



"""Restituisce un bool che corrisponde a ``\\vec x \\cdot \\vec w > 0``
"""
function classification(weights::Array{Float64, 1}, data::Array{Float64, 1})
    appo = 0.
    for i in 1:n
        appo += weights[i] * data[i];
    end
    return appo > 0.
end


"""Ottiene i nuovi pesi a partire da quelli vecchi e dal risultato della
classificazione (che potrebbe essere sbagliato per motivi letterlalmente
casuali).
"""
function get_new_weights(data::Array{Float64, 1}, weights::Array{Float64, 1},
                         to_change::Bool, real_class::Bool)
    @debug "Dato da classificare" data
    @debug "Pesi subito prima del cambio: " weights
    if !to_change
        retval = cos(θ_1)*weights;
        @debug "Non devo cambiarli, moltiplico per il coseno: " retval
        return retval;
    else
        segno = real_class ? 1 : -1;
        retval = (segno.*cos(θ_1).*sin(α)*data + cos(θ_2).*cos(α).*weights);
        @debug "Devo cambiarli: viene" retval
        return retval;
    end
end


function plot_perceptron(training_labels, training_data, weights)
    counter = 0;
    for i in 1:N
        if training_labels[i]
            counter += 1;
        end
    end
    class1 = Array{Float64, 2}(undef, n, counter)
    class2 = Array{Float64, 2}(undef, n, N - counter)
    counter1 = 0;
    counter2 = 0;
    for i in 1:N
        if training_labels[i]
            class1[:, counter1 + 1] = training_data[:, i];
            counter1 += 1;
        else
            class2[:, counter2 + 1] = training_data[:, i];
            counter2 += 1;
        end
    end

    x = range(-20., stop=20., length=1000)
    m = -weights[2, N + 1]/weights[1, N + 1]

    @debug "Class 1: " class1[1, :] class1[2, :]
    @debug "Class 2: " class2[1, :] class2[2, :]
    PyPlot.plot(
        class1[1, :], class1[2, :],
        linestyle="", marker="+",
        color="tab:blue",
    )
    PyPlot.plot(
        class2[1, :], class2[2, :],
        linestyle="", marker="+",
        color="tab:orange",
    )
    PyPlot.plot(
        weights[1, :], weights[2, :], color="tab:green",
        label="Weight evolution", linestyle="-"
    )
    PyPlot.plot(
        x, x.*m, linestyle="-", marker="", color="red"
    )
    PyPlot.suptitle("Quantum 2D perceptron", fontsize=20)
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    PyPlot.legend()
    PyPlot.rc("font", size=16)
    PyPlot.tick_params(labelsize=16)
    PyPlot.matplotlib.rcParams["axes.titlesize"] = 20
    PyPlot.show()
end

"""Calcola quanti errori commette il percettrone nella situazione finale.
Questo calcolo è quello "classico" nel senso che conta quanti punti stanno dalla
parte sbagliata dell'iperpiano, non tiene conto del fatto che la classificazione
è probabilistica.
"""
function count_errors(training_labels::Array{Bool, 1},
                      training_data::Array{Float64, 2},
                      weights::Array{Float64, 1})
    error_counter = 0;
    for i in 1:N
        class = classification(weights, training_data[:, i])
        @debug "Classificazione del punto " training_data[:, i] " con pesi " weights "Viene " class
        if class != training_labels[i]
            error_counter += 1;
        end
    end
    return error_counter;
end


if abspath(PROGRAM_FILE) == @__FILE__
    rng = MersenneTwister(1234);
    weights = Array{Float64, 2}(undef, n, N + 1)
    fill!(weights[:, 1], 0.)

    training_data, training_labels = generate_data()

    measure_miss_counter = 0
    # Training
    @time for i in 1:N
        if i % 10000 == 0
            @info @sprintf("Iteration %d/%d", i, N)
            @info "Pesi in questo momento: " weights[:, i]
        end
        x1, x2 = get_mixed_components(training_data[:, i], weights[:, i])
        prob = success_probability(x1, x2, 1.)
        # Controlla se la misura fornisce il risultato
        # vero o se farlocca
        succ = (rand(rng, Float64) < prob)
        @debug "La misura ha avuto successo?" succ
        if !succ
            global measure_miss_counter += 1;
        end
        # Risultato di θ(w cdot x), flippato se non c'è successo
        class = classification(training_data[:, i], weights[:, i]) ^ !succ
        to_change = (class != training_labels[i])
        weights[:, i + 1] = get_new_weights(
            training_data[:, i], weights[:, i], to_change, training_labels[i])
        # Non faccio update ai vecchi dati perché per ora non li riuso.
    end
    conta_errori = count_errors(training_labels, training_data, weights[:, N + 1])
    println("Pesi finali", weights[:, N + 1])
    println("Norma dei pesi finali: ", norm(weights[:, N + 1]))
    @printf("Le misure quantistiche hanno toppato nel %.1f%% dei casi\n", 100*measure_miss_counter/N)
    @printf("Errori classici commessi (il separatore è quello giusto) %d/%d\n", conta_errori, N)


    # Classificazione dopo
    error_counter = 0
    @time for i in 1:N
        if i % 10000 == 0
            @info @sprintf("Iteration %d/%d", i, N)
            @info "Pesi in questo momento: " weights[:, i]
        end
        x1, x2 = get_mixed_components(training_data[:, i], weights[:, i])
        prob = success_probability(x1, x2, 1.)
        # Controlla se la misura fornisce il risultato
        # vero o se farlocca
        succ = (rand(rng, Float64) < prob)
        @debug "La misura ha avuto successo?" succ
        if !succ
            global measure_miss_counter += 1;
        end
        # Risultato di θ(w cdot x), flippato se non c'è successo
        class = classification(training_data[:, i], weights[:, i]) ^ !succ
        to_change = (class != training_labels[i])
        if to_change
            global error_counter += 1
        end
    end
    @printf("Errori quantistici di classificazione su una copia degli stati di training (dovuti al fatto che le misure fanno schif) %d/%d\n" , error_counter, N)



    if n == 2
        plot_perceptron(training_labels, training_data, weights)
    end
end
