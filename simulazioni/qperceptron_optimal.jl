#!/usr/bin/env julia

using LinearAlgebra;
import PyPlot;
import Optim;
import DelimitedFiles;
using Printf;
using PyCall;


# https://github.com/JuliaPy/PyCall.jl/issues/48
# https://stackoverflow.com/questions/52266203/unable-to-boot-juno
pushfirst!(PyVector(pyimport("sys")["path"]), "")
plot_database = pyimport("plot_database")

include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    error_probability, helstrom_success, log_approx_helstrom_error



# Totale di punti di training generati (e in questo caso anche di passi dell'update)
const N = 30;
const data1 = -2.0;
const data2 = -data1;
const d = abs(data1)
const σ = 1.;
const lattice_size = 100;
const lattice_extension = 3.5

all_parameters = Dict(
    "N" => N,
    "data1" => data1,
    "data2" => data2,
    "sigma" => σ,
    "lattice_size" => lattice_size,
)

"""
Il reticolo va da -lattice_extension*d a lattice_extension*d e contiene lattice_size punti.
"""
function lattice_idx_to_pos(idx::Int64)
    return -lattice_extension*d + 2*lattice_extension*d/lattice_size*(idx - 1)
end


function lattice_pos_to_idx(pos::Float64)
    retval = (pos + lattice_extension*d)/(2lattice_extension*d)*lattice_size

    if !(0 <= retval <= lattice_size - 1)
        strerr = @sprintf("Out of bounds (0, %d)", lattice_size - 1)
        # throw(DomainError(retval, strerr))
        # @warn "Out of bounds" strerr, retval
        if retval < 0
            retval = 0
        else
            retval = lattice_size - 1
        end
    end
    return trunc(Int, retval) + 1
end



function get_mixed_components(θ1::Float64, θ2::Float64, w::Float64)
    r1 = (sin(θ1)*data1 + sin(θ2)*w)^2/2
    r2 = (sin(θ1)*data1 - sin(θ2)*w)^2/2
    return r1, r2
end


function next_w(old_w::Float64, θ1::Float64, θ2::Float64, α::Float64, sign::Bool)
    ss = sign ? 1 : -1;
    c = ss*cos(θ1)*sin(α)*data1 + cos(θ2)*cos(α)*old_w
    @debug "Cambio pesi:" c
    return c
end


function to_minimize(cache, iteration, widx, x)
    θ1 = x[1]
    θ2 = x[2]
    α = x[3]
    w_k = lattice_idx_to_pos(widx)
    x1, x2 = get_mixed_components(θ1, θ2, w_k)
    p = error_probability(x1, x2, 1.)
    prev_wp = lattice_pos_to_idx(next_w(w_k, θ1, θ2, α, true))
    prev_wm = lattice_pos_to_idx(next_w(w_k, θ1, θ2, α, false))

    a = cache[prev_wp, iteration + 1]
    b = cache[prev_wm, iteration + 1]
    # Approssimazione logaritmo al primo ordine.
    if (1 - p) < 0.001
        @debug "Usato approssimazione primo ordine"
        return a - (1 - p)*(1 - exp(b - a))
    end
    return log(p*exp(a) + (1 - p)*exp(b))
end


"""Questo permette di plottare come grafico 2d colorato la DP.
In questo modo si capisce qualcosa anche quando la dimensione del
reticolo e del training set è grossa.
"""
function plot2d_evoluzione(cache)
    X = Array{Array{Float64, 1}, 1}(undef, lattice_size)
    Y = Array{Array{Float64, 1}, 1}(undef, lattice_size)
    for i in 1:lattice_size
        X[i] = fill(0., N)
        Y[i] = fill(0., N)
        for j in 1:N
            X[i][j] = N - j + 1
            Y[i][j] = lattice_idx_to_pos(i)
        end
    end

    PyPlot.figure(2)
    PyPlot.contourf(X, Y, cache, levels=30)
    PyPlot.title("Evoluzione del successo")
    PyPlot.minorticks_on()
    PyPlot.plot(
        N:-1:1, fill(0., N),
        linestyle="--", color="tab:red", label="Valore interessante"
    )
    PyPlot.xlabel("Training set size")
    PyPlot.ylabel("Valore di \$w\$")
    PyPlot.legend()
    PyPlot.colorbar()
    PyPlot.grid("gray")
    plot_database.save_plot(
        title="Percettrone ottimo evoluzione",
        file_path_base="optimal_perceptron_evolution",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )

end



function optimalθ()
    cache = Array{Float64, 2}(undef, lattice_size, N)
    pointers = Array{Array{Float64, 1}, 2}(undef, lattice_size, N)

    # Riempi l'ultima riga
    @time for idx in 1:lattice_size
        x1, x2 = get_mixed_components(π/4, π/4, lattice_idx_to_pos(idx))
        prob = error_probability(x1, x2, 1.)
        cache[idx, N] = log(prob)
        pointers[idx, N] = fill(1., 3)
        @debug "Mixed components" x1 x2 idx prob
    end

    # Ora la dp
    for i in N-1:-1:1
        @info @sprintf("Iterazione %d/%d", i, N)
        @time for idx in 1:lattice_size
            # Qui adesso ho fissato w_k e devo trovare il minimo
            res = Optim.optimize(x -> to_minimize(cache, i, idx, x), pointers[idx, i + 1])
            minarg = Optim.minimizer(res)
            minval = Optim.minimum(res)
            @debug "Minarg and minval" minarg minval
            cache[idx, i] = minval
            pointers[idx, i] = minarg
        end
    end
    # Adesso bisogna stampare il risultato
    @info "Result log error probability" cache[lattice_pos_to_idx(0.), 1]
    open("julia-plots/perceptron_optimal_dp.txt", "w") do io
        DelimitedFiles.writedlm(io, cache, "\t")
    end
    # display("text/plain", cache)
    @info "All table" cache
    @info "Minargs ptrs" pointers
    @debug "Last row" cache[:, N]
    errp = log_approx_helstrom_error(complex(data1), complex(data2))
    hel = fill(errp, N)
    @info "Helstrom error probability" errp

    successione = reverse(fill(1, N) + cache[lattice_pos_to_idx(0.), :])
    PyPlot.figure(1)

    PyPlot.plot(1:N, successione, linestyle="-", marker="", color="tab:blue")
    PyPlot.plot(1:N, hel, linestyle="--", marker="", color="tab:blue", label="Helstrom bound")

    PyPlot.title("Probabilità di errore ottima")
    PyPlot.grid("gray")
    PyPlot.xlabel("Training data size")
    PyPlot.ylabel("Probabilità errore")
    # PyPlot.yscale("log")
    PyPlot.minorticks_on()
    # PyPlot.legend()
    plot_database.save_plot(
        title="Errore ottimo percettrone",
        file_path_base="perceptron_errore_ottimo",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )
    return cache
end




if abspath(PROGRAM_FILE) == @__FILE__
    cache = optimalθ()
    plot2d_evoluzione(cache)
    PyPlot.show()

end
