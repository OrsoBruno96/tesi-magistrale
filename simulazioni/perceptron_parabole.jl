#!/usr/bin/env julia

using LinearAlgebra;
import PyPlot;
using Printf;
using PyCall;


# https://github.com/JuliaPy/PyCall.jl/issues/48
# https://stackoverflow.com/questions/52266203/unable-to-boot-juno
pushfirst!(PyVector(pyimport("sys")["path"]), "")
plot_database = pyimport("plot_database")

include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    helstrom_success


# Dimensione di R^n
const n = 3;
# Totale di punti di training generati (e in questo caso anche di passi dell'update)
const N = 2000;

# Le seguenti variabili sono definite sul logbook e indicano quanto pezzo viene
# usato per il training e quanto conservato
const θ1 = 0.1;
const θ2 = 0.1;
const α = 0.1;
const data1 = -3.0;
const data2 = -data1;
const distance = abs(data1 - data2)
const σ = 1.;
const initial = -5.; # Trucco
const size = 20

all_parameters = Dict(
    "R^n_dimension" => n,
    "N"=> N,
    "theta_1"=> θ1,
    "theta_2"=> θ2,
    "data1"=> data1,
    "data2"=> data2,
    "sigma"=> σ,
    "initial"=> initial,
    "size"=> size,
)

function get_σ_k(k::Int64)
    if isapprox(σ, 1.)
        return 1.
    end
    a = cos(α)^2*cos(θ2)^2
    b = sin(α)^2*(cos(θ1)^2*σ + sin(θ1)^2) + cos(α)^2*sin(θ2)^2;
    return a^k + b*(1 - a^(k + 1))/(1 - a)
end

@inline function get_data(bdata::Bool)
    return bdata ? data1 : data2;
end


"""
Funzione che restituisce le componenti mischiate con il BS gate al 50%
e la sigma corrispondente.
"""
function get_mixed_components(weights::Float64, data::Float64, n::Int64)
    x1 = (sin(θ1)*data + sin(θ2)*weights)/sqrt(2.)
    x2 = (sin(θ1)*data - sin(θ2)*weights)/sqrt(2.)
    x1 = x1^2
    x2 = x2^2
    σk = get_σ_k(n)
    σa = (sin(θ1)^2 *σ + cos(θ1)^2 + cos(θ2)^2 * σk + sin(θ2)^2)/2
    return x1, x2, σa
end


function plot_2d_successo_spoglio()
    l = 20
    start = 0.
    stop = 10.
    x1 = range(start, stop=stop, length=l)
    x2 = range(start, stop=stop, length=l)
    grid = Array{Array{Float64, 1}, 1}(undef, l)
    X = Array{Array{Float64, 1}, 1}(undef, l)
    Y = Array{Array{Float64, 1}, 1}(undef, l)
    for i in 1:l
        grid[i] = fill(0., l)
    end
    @time for i in 1:l
        @info @sprintf("Iterazione %d/%d", i, l)
        X[i] = fill(0., l)
        Y[i] = fill(0., l)
        for j in 1:l
            grid[i][j] = success_probability(x1[i], x2[j], σ)
            X[i][j] = x1[i]
            Y[i][j] = x2[j]
        end
    end
    @debug "Griglia" grid
    @debug "meshgrid" X Y
    @debug "Percorso" x y
    PyPlot.figure(1)
    PyPlot.contourf(X, Y, grid, levels=30)
    PyPlot.title("Success probability")
    PyPlot.colorbar(ticks=range(0., stop=1., step=0.1))

    PyPlot.xlim(start, stop)
    PyPlot.ylim(start, stop)
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    plot_database.save_plot(
        title="Percettrone successo colorato",
        file_path_base="percettrone_successo_colorato",
        parameters=all_parameters,
        script_filename=@__FILE__
    )
end


function plot_2d_successo()
    l = 20
    start = 0.
    stop = 10.
    x1 = range(start, stop=stop, length=l)
    x2 = range(start, stop=stop, length=l)
    grid = Array{Array{Float64, 1}, 1}(undef, l)
    X = Array{Array{Float64, 1}, 1}(undef, l)
    Y = Array{Array{Float64, 1}, 1}(undef, l)
    for i in 1:l
        grid[i] = fill(0., l)
    end
    @time for i in 1:l
        @info @sprintf("Iterazione %d/%d", i, l)
        X[i] = fill(0., l)
        Y[i] = fill(0., l)
        for j in 1:l
            grid[i][j] = success_probability(x1[i], x2[j], σ)
            X[i][j] = x1[i]
            Y[i][j] = x2[j]
        end
    end
    @debug "Griglia" grid
    @debug "meshgrid" X Y
    @debug "Percorso" x y
    PyPlot.figure(2)
    PyPlot.contourf(X, Y, grid, levels=30)
    PyPlot.title("Success probability")
    PyPlot.colorbar(ticks=range(0., stop=1., step=0.1))

    # Ora facciamo le parabole
    for d in range(1., stop=10., length=5)
        size = 100
        xx = fill(0., size)
        yy = fill(0., size)
        w = range(-100, 100, length=size)
        for i in 1:size
            xx[i], yy[i], σ = get_mixed_components(w[i], d, 1)
        end
        PyPlot.plot(
            xx, yy, label="\$d = $d\$",
            linestyle="-", marker="",
        )
    end
    PyPlot.xlim(start, stop)
    PyPlot.ylim(start, stop)
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    PyPlot.legend()
    plot_database.save_plot(
        title="Percettrone parabole",
        file_path_base="percettrone_parabole",
        parameters=all_parameters,
        script_filename=@__FILE__
    )
end




function plot_successo_lineare()
    PyPlot.figure(3, figsize=(10, 9))
    PyPlot.rc("font", size=16)
    PyPlot.title("Success probability")
    for d in range(1., stop=10., length=5)
        size = 100
        w = range(-35, 35, length=size)
        p = fill(0., size)
        for i in 1:size
            x1, x2, σ = get_mixed_components(w[i], d, 1)
            p[i] = success_probability(x1, x2, σ)
        end
        PyPlot.plot(w, p, label="d = \$$d\$")
        PyPlot.grid("gray")
        PyPlot.minorticks_on()
        PyPlot.legend()
        PyPlot.xlabel("Weight norm", fontsize=14)
        PyPlot.ylabel("Success probability", fontsize=14)
    end
    plot_database.save_plot(
        title="Percettrone successo limite matematico",
        file_path_base="percettrone_successo_limite_matematico",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )

end


if abspath(PROGRAM_FILE) == @__FILE__
    plot_2d_successo_spoglio()
    plot_2d_successo()
    plot_successo_lineare()
    PyPlot.show()
end
