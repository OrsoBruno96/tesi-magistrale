#!/usr/bin/env julia

using LinearAlgebra;
import PyPlot;
using Printf;
using PyCall;


# https://github.com/JuliaPy/PyCall.jl/issues/48
# https://stackoverflow.com/questions/52266203/unable-to-boot-juno
pushfirst!(PyVector(pyimport("sys")["path"]), "")
plot_database = pyimport("plot_database")

include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    helstrom_success

"""Simulazione del percettrone con molti più dettagli.
Le differenze rispetto al precedente sono le seguenti:
- Ora si tiene in considerazione la σ degli stati.
- L'algoritmo di update è diverso
- Gli stati sono termici displaced.
"""


#####################
# Parametri globali #
#####################


# Dimensione di R^n
const n = 3;
# Totale di punti di training generati (e in questo caso anche di passi dell'update)
const N = 2000;

# Le seguenti variabili sono definite sul logbook e indicano quanto pezzo viene
# usato per il training e quanto conservato
const θ1 = 0.1;
const θ2 = 0.1;
const α = 0.1;
const data1 = -3.0;
const data2 = -data1;
const distance = abs(data1 - data2)
const σ = 1.;
const initial = -5.; # Trucco
const size = 20

all_parameters = Dict(
    "R^n_dimension" => n,
    "N"=> N,
    "theta_1"=> θ1,
    "theta_2"=> θ2,
    "data1"=> data1,
    "data2"=> data2,
    "sigma"=> σ,
    "initial"=> initial,
    "size"=> size,
)


function get_σ_k(k::Int64)
    if isapprox(σ, 1.)
        return 1.
    end
    a = cos(α)^2*cos(θ2)^2
    b = sin(α)^2*(cos(θ1)^2*σ + sin(θ1)^2) + cos(α)^2*sin(θ2)^2;
    return a^k + b*(1 - a^(k + 1))/(1 - a)
end


@inline function get_data(bdata::Bool)
    return bdata ? data1 : data2;
end


"""
Funzione che restituisce le componenti mischiate con il BS gate al 50%
e la sigma corrispondente.
"""
function get_mixed_components(bdata::Bool, weights::Float64, n::Int64)
    data = get_data(bdata)
    x1 = (sin(θ1)*data + sin(θ2)*weights)/sqrt(2.)
    x2 = (sin(θ1)*data - sin(θ2)*weights)/sqrt(2.)
    x1 = x1^2
    x2 = x2^2
    σk = get_σ_k(n)
    σa = (sin(θ1)^2 *σ + cos(θ1)^2 + cos(θ2)^2 * σk + sin(θ2)^2)/2
    return x1, x2, σa
end




function evoluzione_w_k()
    # w = Array{Float64, 1}(undef, N + 1);
    weights = Array{Float64, 1}(undef, N + 1);
    # I pesi al quadrato per calcolare la varianza
    weights2 = Array{Float64, 1}(undef, N + 1);
    # Varianza
    var = Array{Float64, 1}(undef, N + 1);

    derivata = Array{Float64, 1}(undef, N);
    prob = Array{Float64, 1}(undef, N);
    weights[1] = initial;
    weights2[1] = initial;
    var[1] = 0;
    posx = Array{Float64, 1}(undef, N);
    posy = Array{Float64, 1}(undef, N);

    c = cos(α)*cos(θ1)
    s = sin(α)*cos(θ2)
    @time for i in 1:N
        if i % 10 == 0
            @info @sprintf("Iteration %d/%d", i, N)
            @info "Pesi in questo momento" weights[i] weights2[i] var[i]
        end
        x1, x2, σ = get_mixed_components(true, weights[i], i)
        posx[i] = x1
        posy[i] = x2
        p = success_probability(x1, x2, σ)
        @debug "Success probability" x1 x2 σ p
        weights[i + 1] = c*weights[i] + s*(2*p - 1)*data1
        weights2[i + 1] = c^2*weights2[i] + s^2*data1^2 + 2*s*c*data1*(2*p - 1)*weights[i]
        var[i + 1] = abs(weights2[i + 1] - weights[i]^2)
        derivata[i] = weights[i + 1] - weights[i]
        prob[i] = 1 - p
    end
    if N < 100
        @info "Pesi, varianze" weights var
    end
    fig, ax1 = PyPlot.subplots()
    ax1.set_xlabel("Iteration")
    ax1.set_ylabel("Expected weight norm")
    ax1.errorbar(
        0:N, weights, sqrt.(var), linestyle="", marker="+",
        color="tab:blue", label="\$w_k\$"
    )
    ax2 = ax1.twinx()
    # ax2.plot(
    #     1:N, derivata, linestyle="", marker="+",
    #     color="tab:orange", label="\$w_{k+1} - w_k\$"
    # )
    ax2.set_ylabel("Error probability")
    hel_err = 1 - helstrom_success(complex(data1), complex(data2))
    @info "Probabilità di errore helstrom bound:" hel_err
    hel = fill(hel_err, N)
    ax2.plot(
        1:N, hel, linestyle="--", marker="", color="tab:red",
        label="Helstrom bound",
    )
    ax2.plot(
        1:N, prob, linestyle="", marker="+",
        color="tab:green", label="\$p(w_k)\$"
    )
    ax2.set_yscale("log")
    ax1.set_title("Expected weight value with \$d = $distance\$")
    ax1.grid("gray")
    ax1.minorticks_on()
    ax2.minorticks_on()
    fig.legend()
    plot_database.save_plot(
        title="Percettrone andamento w_k",
        file_path_base="perceptron_andamento_w_k",
        parameters=all_parameters,
        script_filename=@__FILE__
    )
    return posx, posy
end




function distribuzione_photocounting_tipo_poisson()
    totale = 0
    distribuzione = Array{Float64, 1}(undef, size + 1)
    for i in 0:size
        distribuzione[i + 1] = therm_disp_nρn(i, 3., σ)
        totale += distribuzione[i + 1]
        @info i distribuzione[i + 1] totale
    end
    PyPlot.title("Photocounting distribution \$\\sigma_{th} = $σ_th\$")
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    PyPlot.legend()
    PyPlot.plot(
        0:size, distribuzione,
        linestyle="", marker="+",
        color="tab:blue"
    )
end


function plot_2d_successo(xx, yy)
    l = 20
    start = 0.
    stop = 10.
    x1 = range(start, stop=stop, length=l)
    x2 = range(start, stop=stop, length=l)
    grid = Array{Array{Float64, 1}, 1}(undef, l)
    X = Array{Array{Float64, 1}, 1}(undef, l)
    Y = Array{Array{Float64, 1}, 1}(undef, l)
    for i in 1:l
        grid[i] = fill(0., l)
    end
    @time for i in 1:l
        @info @sprintf("Iterazione %d/%d", i, l)
        X[i] = fill(0., l)
        Y[i] = fill(0., l)
        for j in 1:l
            grid[i][j] = success_probability(x1[i], x2[j], σ)
            X[i][j] = x1[i]
            Y[i][j] = x2[j]
        end
    end
    @debug "Griglia" grid
    @debug "meshgrid" X Y
    @debug "Percorso" x y
    PyPlot.figure(2)
    PyPlot.contourf(X, Y, grid, levels=30)
    PyPlot.title("Success probability with \$d = $distance\$")
    PyPlot.colorbar(ticks=range(0., stop=1., step=0.1))
    PyPlot.plot(
        xx, yy, color="tab:red", label="Path",
        linestyle="--", marker="+",
    )
    PyPlot.xlim(start, stop)
    PyPlot.ylim(start, stop)
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    PyPlot.legend()
    plot_database.save_plot(
        title="Percettrone prob successo e percorso",
        file_path_base="thermal_displaced_perceptron_photocounting",
        parameters=all_parameters,
        script_filename=@__FILE__
    )
end



if abspath(PROGRAM_FILE) == @__FILE__
    # distribuzione_photocounting_tipo_poisson()
    x, y = evoluzione_w_k()
    plot_2d_successo(x, y)
    PyPlot.show()
end
