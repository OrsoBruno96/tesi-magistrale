#!/usr/bin/env python3

from matplotlib import pyplot as plt
from numpy import linspace, arccos, tan
from scipy.optimize import fsolve


def func(x, n):
    return n*x**n*(1 - x**2) - 1 + x**(n + 1)


def peso(x, n):
    alpha = arccos(x)
    return (1 - x**(n + 1))/tan(alpha/2)


def plot_funzione(fignum, start, stop):
    plt.figure(fignum, figsize=(10, 9))
    x = linspace(-1, 1, 1000)
    for n in range(start, stop):
        y = func(x, n)
        plt.plot(x, y, label=f"$n = {n}$")
    y = linspace(0, 0, 1000)
    plt.plot(x, y, color='red', linestyle='--')
    plt.ylim(-1, .5)
    plt.xlim(.5, 1)
    plt.rc('font', size=16)
    plt.title("Equation solution")
    plt.xlabel("$x$", fontsize=14)
    plt.ylabel("$f(x)$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    plt.savefig("plots/equazione_stupida_numerica.pdf")


def trova_soluzioni(fignum, start, stop):
    solutions = []
    base = list(range(start, stop))
    for n in base:
        def lafunc(x):
            return func(x, n)
        solutions.append(fsolve(lafunc, 0.8))
    weights = [peso(s, n) for n, s in zip(base, solutions)]
    plt.figure(fignum, figsize=(10, 9))
    plt.plot(base, weights, label="Factor", linestyle='', marker='o')
    plt.rc('font', size=16)
    plt.title("Enhancement factor")
    plt.xlabel("$n$", fontsize=14)
    plt.ylabel("$f(x)$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    plt.savefig("plots/equazione_stupida_numerica_valore_soluzioni.pdf")


if __name__ == "__main__":
    plot_funzione(1, 3, 12)
    trova_soluzioni(2, 3, 12)
    plt.show()
