from sqlalchemy import Column, Integer, String, DateTime, create_engine, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select

from matplotlib import pyplot as plt
from json import dumps
from hashlib import md5
from os.path import join, dirname, relpath, commonprefix
from os import makedirs, system, remove
from logging import getLogger, basicConfig, INFO
from datetime import datetime, date
from inspect import stack

from settings import database_url, plot_dir, BASE_DIR


logger = getLogger(__file__)
basicConfig(level=INFO)
Base = declarative_base()
engine = create_engine(database_url)
Session = sessionmaker(bind=engine)


class Plot(Base):
    __tablename__ = "plots"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    file_path = Column(String)
    script_filename = Column(String)
    parameters = Column(String)
    timestamp = Column(DateTime)

    def __repr__(self):
        return f"<Plot(title={self.title}>"

    def __str__(self):
        return f"{self.title} - {self.script_filename} - {self.file_path}"

    @classmethod
    def migrate(self):
        Base.metadata.create_all(bind=engine)
        # check table exists
        ins = inspect(engine)
        for _t in ins.get_table_names():
            print(_t)


pt = Plot.__table__


def file_md5(fname):
    """Calcola l'md5 di un file a partire dal suo nome.
    """
    hash_md5 = md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def png_name_from_pdf(filename: str):
    return f"{filename}-1.png"


def show_plots(query):
    """Chiama eog con i png corrispondenti alla query fatta.
    Presuppone che tutti i plot siano stati convertiti in png
    con
    `for file in $(ls *.pdf) ; do pdftoppm $file $file -png ; done`
    Per installare `pdftoppm` serve `poppler-utils`
    """
    query = select([Plot.__table__.c.file_path]).where(query)
    s = Session()
    res = list(map(lambda x: join(
        dirname(BASE_DIR),
        png_name_from_pdf(x[0])), s.execute(query).fetchall()))
    command = "eog " + " ".join(res)
    system(command)


def merge_databases():
    """Merge asimmetrico dei dati in due database.
    """
    raise NotImplementedError()


def default_json_decoder(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()


def purge_plots(query):
    """Filtra i plot nella query e cancella le entries corrispondenti.
    Cancella anche i file con i plot corrispondenti.
    """
    query = select(
        [pt.c.id, pt.c.file_path, pt.c.title, pt.c.timestamp, pt.c.parameters]
    ).where(query).order_by(pt.c.timestamp)

    s = Session()
    res = s.execute(query).fetchall()
    print(dumps([dict(row) for row in res], indent=4, default=default_json_decoder))
    continua = input("Vuoi davvero cancellarli? (y/s/n) ")
    if continua != 'y' and continua != 's':
        print("Annullamento operazione.")
        return
    individual = continua == 's'
    for r in res:
        if individual:
            print(dumps(dict(r), indent=4, default=default_json_decoder))
            c = input("Questo lo cancelliamo? (y/n)")
            if c != 'y':
                continue
        filename = join(dirname(BASE_DIR), r[1])
        logger.info(f"Deleting file {filename}")
        # Plot.query.filter_by(id=r[0]).delete()
        # remove(filename)
    # s.commit()


def save_plot(
        title: str, file_path_base: str, parameters,
        script_filename=None):
    if script_filename is None:
        try:
            frame = stack()[1]
        except IndexError:
            raise RuntimeError(
                "Si è rotto python 'inspect.stack()'. Stai"
                " forse chiamando python da Julia e non hai"
                "messo il parametro 'script_filename'?"
            )
        script_filename = frame[0].f_code.co_filename
    temp_filename = "/tmp/plot_cusumano.pdf"
    plt.savefig(temp_filename)
    fmd5 = file_md5(temp_filename)
    path = join(plot_dir, f"{file_path_base}_{fmd5}.pdf")
    makedirs(plot_dir, exist_ok=True)
    plt.savefig(path)
    logger.info(f"Saved plot '{title}' to path '{path}'")
    logger.debug(f"I try to open a connection to database")
    session = Session()
    logger.debug("Opened connection to database")
    database_filepath = relpath(path, commonprefix([dirname(BASE_DIR), path]))
    plot_object = Plot(
        title=title, file_path=database_filepath, script_filename=script_filename,
        parameters=dumps(parameters), timestamp=datetime.now(),
    )
    session.add(plot_object)
    session.commit()
    logger.info(f"Correctly saved plot '{title}' to database.")
