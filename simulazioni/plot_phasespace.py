#!/usr/bin/env python3
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from matplotlib.ticker import LinearLocator

# Fixing random state for reproducibility
np.random.seed(19680801)


def randrange(n, vmin, vmax):
    '''
    Helper function to make an array of random numbers having shape (n, )
    with each number distributed Uniform(vmin, vmax).
    '''
    return (vmax - vmin)*np.random.rand(n) + vmin

fig = plt.figure()
ax = fig.gca(projection='3d')
# ax = fig.add_subplot(111, projection='3d')

# n = 100

# For each set of style and range settings, plot n random points in the box
# defined by x in [23, 32], y in [0, 100], z in [zlow, zhigh].


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


def gaus2d(x, y, xc, yc):
    return gaussian(x, xc, 1)*gaussian(y, yc, 1)


reticolo_x = np.linspace(-10, 10, 100)
reticolo_y = np.linspace(-10, 10, 100)
reticolo_x, reticolo_y = np.meshgrid(reticolo_x, reticolo_y)
z1 = gaus2d(reticolo_x, reticolo_y, 0, 0)
z2 = gaus2d(reticolo_x, reticolo_y, 8, 0)


surf = h1 = ax.plot_surface(reticolo_x, reticolo_y, z1 + z2,
                            cmap=cm.coolwarm, linewidth=0, antialiased=True)
ax.set_zlim(0, 2)
ax.zaxis.set_major_locator(LinearLocator(20))

# h2 = ax.plot_surface(reticolo_x, reticolo_y, z2, linewidth=0, antialiased=True)

# fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
