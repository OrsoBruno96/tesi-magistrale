#!/usr/bin/env python3
from matplotlib import pyplot as plt
from numpy import exp, array as nparr, sqrt, linspace

from plot_database import save_plot, show_plots, pt, purge_plots


def test_save_plot():
    plt.figure(1, figsize=(10, 9))
    x = linspace(0, 6, 1000)
    helstrom = 0.5*(1 - sqrt(1 - exp(-x)))
    nostro = 0.5*exp(-x)
    plt.plot(x, nostro, color='blue', label='Nostro')
    plt.plot(x, helstrom, color='red', label='Helstrom bound')
    # plt.plot(x, nostro/2, color='blue', label='Nostro/2', linestyle='--')

    plt.rc('font', size=16)
    plt.title("Oneshot")
    plt.xlabel("Distanza fra i centri (al quadrato)", fontsize=14)
    plt.ylabel("Probabilità di errore", fontsize=14)
    # plt.yscale('log')
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    save_plot(
        title="Oneshot Helstrom", file_path_base="oneshot_helstrom", parameters={},
    )
    # plt.savefig("oneshot.pdf")

    plt.show()


def test_show_plots():
    show_plots(pt.c.file_path.like('%caso_B%'))


def test_purge_plots():
    purge_plots(pt.c.parameters.like('%repetitions": 100,%'))


if __name__ == "__main__":
    # test_save_plot()
    # test_show_plots()
    test_purge_plots()
