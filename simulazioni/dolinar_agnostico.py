#!/usr/bin/env python3
"""Questa volta il nostro algoritmo non saprà quasi niente sugli stati in ingresso,
se non che sono gaussiani. Nel primo caso, faremo con coerenti, poi vediamo di
estendere a gaussiani generici (sembra facile per termici, meno per squeezed).
"""

from matplotlib import pyplot as plt
from numpy import exp, array as nparr, sqrt, pi, zeros
from numpy.random import normal, seed
from logging import getLogger, INFO, DEBUG, basicConfig
from argparse import ArgumentParser
from os.path import join
from plot_database import save_plot
from decouple import config
import numpy as np


logger = getLogger(__file__)


def start_logger(level=INFO):
    """Solo per stampare informazioni di debug.
    """
    basicConfig(level=level)


def lookahead(iterable):
    """Pass through all values from the given iterable, augmented by the
    information if there are more values to come after the current one
    (True), or if it is the last value (False).
    """
    # Get an iterator and pull the first value.
    it = iter(iterable)
    last = next(it)
    # Run the iterator to exhaustion (starting from the second value).
    for val in it:
        # Report the *previous* value (more to come).
        yield last, True
        last = val
    # Report the last value.
    yield last, False


def helstrom_error_prob(d):
    """Restituisce la probabilità di errore minima calcolata con
    l'helstrom bound.
    """
    return .5*(1 - sqrt(1 - exp(-0.5*abs(d)**2)))


def initial_priors(sigma0: float):
    """I priori sono joint priors p(d, A) e p(d, B), per cui
    sono tutte e due PDF. Avere a disposizione funzioni complica
    parecchio il tutto ma dato che se partiamo con gaussiane ci rimangono
    gaussiane, vediamo ci bastano 3 variabili per ognuna. Sono 3 e non 2
    perché il fatto di avere due possibilità, A e B, ci aggiunge una
    ulteriore normalizzazione davanti.
    """
    return nparr([
        (.5, 0., sigma0),
        (.5, 0., sigma0),
    ])


def extract_center(center: tuple, sigma2: float=1):
    """Estrae il risultato di una misura heterodyne sapendo che lo stato
    originale ha centro nel posto opportuno.
    """
    return normal(center[0], sigma2) + 1j*normal(center[1], sigma2)


def get_center(centrato: bool, divisions: int, reald: float):
    """Restituisci la posizione del centro dello stato coerente
    alla fine del circuito.
    """
    if centrato:
        return (0, 0)
    return (reald/sqrt(divisions), 0)


def _newsigma(sigma1, sigma2):
    return (1./sigma1 + 1./sigma2)**(-1)


def _newcenter(c1, c2, sigma1, sigma2, newsigma=None):
    newsigma = newsigma if newsigma is not None else _newsigma(sigma1, sigma2)
    return (c1/sigma1 + c2/sigma2)*newsigma


def _newexponent(c1, c2, sigma1, sigma2, newsigma=None):
    newsigma = newsigma if newsigma is not None else _newsigma(sigma1, sigma2)
    return .5*(abs(c1/sigma1 + c2/sigma2)**2*newsigma - (abs(c1)**2/sigma1 + abs(c2)**2/sigma2))


def _newnorm(c1, c2, sigma1, sigma2, newsigma=None):
    newsigma = newsigma if newsigma is not None else _newsigma(sigma1, sigma2)
    return exp(_newexponent(c1, c2, sigma1, sigma2, newsigma))/(2*pi*(sigma1 + sigma2))


def _gaus(var, center, sigma):
    return 1./(2*pi)*exp(-abs(var-center)**2/(2*sigma))


def compute_posteriors(misura, oldpriors: tuple, swapped: bool):
    """Questa funzione è il cancro più assoluto.
    """
    if not swapped:
        newsigma = _newsigma(1, oldpriors[1][2])
        newcenter = _newcenter(misura, oldpriors[1][1], 1, oldpriors[1][2], newsigma)
        stupido = oldpriors[0][0]*_gaus(misura, 0, 1)
        altranorm = oldpriors[1][0]*_newnorm(
            misura, oldpriors[1][1], 1, oldpriors[1][2], newsigma)
        anorm = stupido/(stupido + altranorm)
        aanorm = altranorm/(stupido + altranorm)
        retval = (
            (anorm, oldpriors[0][1], oldpriors[0][2]),
            (aanorm, newcenter, newsigma),
        )
    else:
        newsigma = _newsigma(oldpriors[0][2], 1)
        newcenter = _newcenter(oldpriors[0][1], misura, oldpriors[0][2], 1, newsigma)
        stupido = oldpriors[1][0]*_gaus(misura, 0, 1)
        altranorm = oldpriors[0][0]*_newnorm(
            oldpriors[0][1], misura, oldpriors[0][2], 1, newsigma)
        anorm = stupido/(stupido + altranorm)
        aanorm = altranorm/(stupido + altranorm)
        retval = (
            (aanorm, newcenter, newsigma),
            (anorm, oldpriors[1][1], oldpriors[1][2]),
        )
    logger.debug(f"newsigma: {newsigma}")
    logger.debug(f"newcenter: {newcenter}")
    logger.debug(f"Stupido: {stupido}, altranorm: {altranorm}")
    return retval


def plot_andamento_posteriori(
        A, B, fignum, caso, reald, sdevA=None, sdevB=None, logy: bool=False,
        savename: str=None):
    plt.figure(fignum)
    plt.title(f"Posteriors case {caso}")
    plt.errorbar(
        range(len(A)), A, sdevA,
        label='Posterior for A', marker='x')#, color='tab:blue')
    # plt.plot(
    #     range(len(A)), np.full(len(A), helstrom_error_prob(reald)),
    #     label='Helstrom bound', color='tab:red')
    # plt.errorbar(
    #     range(0, len(B)), B, sdevB,
    #     label='Posteriore per B', marker='x')#, color='tab:orange')
    if logy:
        plt.yscale('log')
    plt.xlabel("Iteration")
    plt.ylabel("Posterior")
    plt.ylim(0, 1)
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    if savename is not None:
        save_plot(
            title="Andamento posteriori con iterazione",
            file_path_base=f"posteriori_agnostico_caso_{caso}",
            parameters=all_parameters,
        )


def plot_asintoti_posteriori(
        val, sigma, fignum, maxdiv, spacing, caso: str, logy: bool=False,
        reald: float=None,
        savename: str=None, no_save: bool=False):
    plt.figure(fignum)
    plt.title(f"Andamento asintoti dei posteriori caso {caso}")
    plt.errorbar(range(0, maxdiv, spacing), val, sigma)
    if reald is not None:
        val = helstrom_error_prob(reald)
        if caso == "A":
            val = 1 - val
        plt.plot(range(0, maxdiv, spacing),
                 np.full(len(range(0, maxdiv, spacing)), val),
                 label='Helstrom bound', color='tab:red')
    if logy:
        plt.yscale('log')
    plt.xlabel('Numero di divisioni')
    if not logy:
        plt.ylim(0, 1)
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    if (savename is not None) and (not no_save):
        save_plot(
            title="Asintoti dei posteriori",
            file_path_base=savename,
            parameters=all_parameters,
        )


def repeat_simulations(maxdiv: int, repetitions: int, caso: str,
                       reald: float, spacing: int, sigma0: float,
                       sigma_th: float=1,
                       no_save: bool=False, **kwargs):
    """Questa funzione andrà modificata per ripetere la simulazione tante
    volte e per farla per diversi valori di 'divisions'
    """
    endprob = zeros([maxdiv // spacing + 1])
    endsigma = zeros([maxdiv // spacing + 1])
    # Servono a fare un istogramma del valore "asintotico" dei posteriori
    # separatamente per ogni divisione scelta
    cumA = zeros([maxdiv // spacing + 1, repetitions])
    cumB = zeros([maxdiv // spacing + 1, repetitions])
    for divisions, not_last in lookahead(range(0, maxdiv, spacing)):
        logger.info(f"Starting with {divisions} divisions")
        A, B, sA, sB = zeros([divisions]), zeros([divisions]), zeros([divisions]), \
            zeros([divisions])
        for _ in range(repetitions):
            if (_ % 100) == 0:
                logger.info(f"Divisions: {divisions}, repeat: {_}")
            swapped = False
            priors = nparr(zeros([divisions + 1, 2, 3], dtype=np.complex_))
            priors[0] = initial_priors(sigma0)
            for i in range(divisions):
                centrato = (caso == "A") ^ swapped
                logger.debug(f"Centrato: {centrato}")
                misura = extract_center(get_center(centrato, divisions, reald), 1 + sigma_th)
                logger.debug(f"Misura: {misura}\npriors: {priors[i]}\nswapped: {swapped}")
                new_posteriors = compute_posteriors(misura, priors[i], swapped)
                priors[i + 1] = new_posteriors
                logger.debug(f"New posteriors: {new_posteriors}")
                swapped = priors[i + 1][0][0] < .5
                logger.debug(f"Swapped: {swapped}")
                A[i] += priors[i + 1][0][0].real
                B[i] += priors[i + 1][1][0].real
                sA[i] += priors[i + 1][0][0].real**2
                sB[i] += priors[i + 1][1][0].real**2
            cumA[divisions // spacing, _] = priors[divisions][0][0].real
            cumB[divisions // spacing, _] = priors[divisions][1][0].real
        A /= repetitions
        B /= repetitions
        sA = sqrt(sA/repetitions - A**2)
        sB = sqrt(sB/repetitions - B**2)
        if not A.any():
            continue
        savename1 = None if (not_last or no_save) else f"caso_{caso}_d_{reald}_verboso"
        plot_andamento_posteriori(
            A, B, fignum=1, caso=caso, reald=reald, sdevA=sA, sdevB=sB, logy=False,
            savename=savename1)
        savename2 = None if (not_last or no_save) else f"caso_{caso}_d_{reald}_verboso_log"
        plot_andamento_posteriori(
            A, B, caso=caso, reald=reald, fignum=2, sdevA=sA, sdevB=sB, logy=True,
            savename=savename2)
        endprob[int(divisions / spacing)] = A[-1]
        endsigma[int(divisions / spacing)] = sA[-1]
    analyze_results(spacing, endprob, endsigma, maxdiv=maxdiv,
                    reald=reald, caso=caso, no_save=no_save)
    plot_posterior_asymptotic_distribution(
        cumA, cumB, maxdiv=maxdiv, no_save=no_save,
        spacing=spacing, repetitions=repetitions,
        caso=caso, reald=reald,
    )


def analyze_results(
        spacing, endp, endsigma, maxdiv,
        reald, caso, no_save=False):
    logger.info(endp)
    logger.info(endsigma)
    plot_asintoti_posteriori(
        endp, endsigma, fignum=3, logy=False,
        savename=f"caso_{caso}_d_{reald}_asintotico.pdf", maxdiv=maxdiv,
        reald=reald,
        spacing=spacing, caso=caso, no_save=no_save,
    )
    plot_asintoti_posteriori(
        endp, endsigma, fignum=4, logy=True,
        savename=f"caso_{caso}_d_{reald}_asintotico_log.pdf",
        reald=reald,
        maxdiv=maxdiv, spacing=spacing, caso=caso, no_save=no_save,
    )


def plot_posterior_asymptotic_distribution(
        cumA, cumB, maxdiv, spacing, repetitions, caso, reald, no_save: bool==False,
):
    for divisions in range(0, maxdiv, spacing):
        plt.figure(18 + divisions)
        plt.title(f"Posteriors distribution for {divisions} divisions")
        plt.hist((cumA[divisions // spacing], cumB[divisions // spacing]),
                 int(sqrt(repetitions)), stacked=True,
                 color=['tab:blue', 'tab:orange'], label=['A', 'B'])
        plt.legend()
        if not no_save:
            save_plot(
                title="Distribuzione posteriori finali",
                file_path_base=f"distribuzione_posteriori_agnostico_caso_{caso}"
                f"_divisioni_{divisions}_distanza_{reald}",
                parameters=all_parameters,
            )


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument(
        "--no-save", help="Vuoi non salvare in db e su file?",
        action='store_true',
    )
    parser.add_argument(
        "--noninteractive", help="Vuoi non mostrare il plot?",
        action='store_true',
    )
    args = vars(parser.parse_args())
    SEED = config("SEED", cast=int, default=1234)
    start_logger(INFO)
    seed(SEED)
    # Temperatura degli stati gaussiani in ingresso
    beta = config("BETA", cast=float, default=100000)
    # Spread della gaussiana dello stato gaussiano
    sigma_th = 1 if beta > 30 else sqrt(1./(1-exp(-beta)))
    all_parameters = {
        # Incertezza sul priore di d.
        'sigma0': config("SIGMA_0", cast=float, default=3.), # In realtà è sigma^2
        'reald': config("REALD", cast=float, default=4),
        'caso':  config("CASO", cast=str, default="A"),
        'maxdiv': config("MAXDIV", cast=int, default=101), # Importante che sia n*repetitions + 1
        'repetitions': config("REPETITIONS", cast=int, default=10),
        'spacing': config("SPACING", cast=int, default=10),
        'no_save': args["no_save"],
        'seed': SEED,
        'sigma_th': sigma_th,
    }
    repeat_simulations(**all_parameters)
    if not args["noninteractive"]:
        plt.show()
