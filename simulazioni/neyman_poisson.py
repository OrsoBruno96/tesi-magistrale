#!/usr/bin/env python3
from matplotlib import pyplot as plt
from numpy import exp, linspace, sqrt
from scipy.stats import poisson


def compute_single_limit(x, CL):
    k = 0
    prob = 0.
    while True:
        prob += poisson.pmf(k, x)
        if prob > CL:
            break
        k += 1
    return k


def helstrom_success(dsq):
    return .5*(1 - sqrt(1 - exp(-0.5*dsq)))


def invert_function(x, y):
    retx = list(range(min(y), max(y) + 1))
    rety = []
    for i in retx:
        for xx, yy in zip(x, y):
            if yy == i:
                rety.append(xx)
                break
    return retx, rety


def plot_neyman():
    stop = 10
    x = linspace(0, stop, 1000)
    y = [compute_single_limit(xx, .95) for xx in x]
    plt.figure(1, figsize=(10, 9))
    plt.plot(x, y, linestyle='', marker='o')
    plt.fill_between(x, 0, y, hatch='/', edgecolor='tab:blue', alpha=0.3)
    plt.xlim(0, stop)
    plt.ylim(0, 15)
    plt.rc('font', size=16)
    plt.title("Neyman construction $CL = 0.95$")
    plt.xlabel("$|d|^2$", fontsize=14)
    plt.ylabel("$n^\dagger$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.savefig("plots/neyman_construction.pdf")
    plt.figure(2, figsize=(10, 9))
    n, dsq = invert_function(x, y)
    prob = [helstrom_success(d) for d in dsq]
    plt.plot(n, prob, linestyle='', marker='o')
    plt.xlim(min(n), max(n))
    plt.ylim(0, .5)
    plt.rc('font', size=16)
    plt.title("Estimated error probability")
    plt.xlabel("$n^\dagger$", fontsize=14)
    plt.ylabel(r"$P_{\mathrm{err}}$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.savefig("plots/neyman_error_prob.pdf")


if __name__ == "__main__":
    plot_neyman()
    plt.show()
