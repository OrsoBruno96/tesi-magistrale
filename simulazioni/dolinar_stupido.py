#!/usr/bin/env python3
"""Simulazione per capire l'efficienza del dolinar con photocounting.
"""
from random import random
from matplotlib import pyplot as plt
from numpy import exp, array as narr, sqrt
from logging import getLogger, Formatter, StreamHandler, INFO, DEBUG

logger = getLogger(__file__)


def prob(modalpha, beta, division):
    return (1-exp(-beta))*exp(-(1-exp(-beta))*modalpha**2/division)

beta = 6  # $\frac{\hbar \omega}{k\ped{B} T}$
modalpha = 3  # $|\alpha|^2$
caso = "B"  # a scelta fra "A" e "B"

iterations = 1000


def swap(a, b):
    return b, a

def posteriors(outcome, priors, a, b):
    """Calcola i posteriori a partire dai priori,
    dal risultato della misura (0, \overline{0}) e
    dai valori di a e b.
    """
    if outcome == 0:
        return (
            a*priors[0]/(a*priors[0] + b*priors[1]),
            b*priors[1]/(a*priors[0] + b*priors[1]),
        )
    else:
        return (
            (1-a)*priors[0]/((1-a)*priors[0] + (1-b)*priors[1]),
            (1-b)*priors[1]/((1-a)*priors[0] + (1-b)*priors[1]),
        )


def plot(A, B, fignum, sdevA=None, sdevB=None):
    plt.figure(fignum)
    plt.title(f"Posteriori caso {caso}")
    plt.errorbar(
        range(0, len(A)), A, sdevA,
        label='Posteriore per A', marker='x')#, color='tab:blue')
    # plt.errorbar(
    #     range(0, len(B)), B, sdevB,
    #     label='Posteriore per B', marker='x')#, color='tab:orange')
    # plt.yscale('log')
    plt.xlabel("Iterazione")
    plt.ylabel("Posteriore")
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()


def simula(fignum, division, do_plot=False):
    a, b = get_a_b(False)
    logger.debug(f"Siamo nel caso {caso}")
    logger.debug(f"a: {a} b: {b}")

    priors = [
        (.5, .5),
    ]
    swapped = False
    for i in range(0, division):
        r = random()
        zeroprob = a if ((caso == "A") ^ swapped) else b
        # zeroprob = b
        outcome = 0 if (r < zeroprob) else 1
        if swapped:
        # if False:
            priors.append(posteriors(outcome, priors[-1], b, a))
        else:
            priors.append(posteriors(outcome, priors[-1], a, b))
        swapped = priors[-1][0] < (priors[-1][1] - 0.2)
    A, B = zip(*priors)
    if do_plot:
        plot(A, B, fignum=fignum)
    return narr(A), narr(B)


def get_a_b(swapped: bool):
    a = prob(0, beta, division)
    b = prob(modalpha, beta, division)
    if swapped:
        return b, a
    return a, b


if __name__ == "__main__":
    logger.setLevel(INFO)
    formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    sh = StreamHandler()
    sh.setLevel(INFO)
    sh.setFormatter(formatter)
    logger.addHandler(sh)
    for i, division in enumerate(range(1, 101, 10)):
        dd = division + 1
        avgsA, sdevsA = narr([0.]*dd), narr([0.]*dd)
        avgsB, sdevsB = narr([0.]*dd), narr([0.]*dd)
        for rep in range(0, iterations):
            A, B = simula(i, division, False)
            avgsA += A
            sdevsA += A**2
            avgsB += B
            sdevsB += B**2
        avgsA /= iterations
        sdevsA /= iterations
        avgsB /= iterations
        sdevsB /= iterations
        sdevsA = sqrt(sdevsA - avgsA**2)
        sdevsB = sqrt(sdevsB - avgsB**2)
        plot(avgsA, avgsB, 1, sdevsA/3, sdevsB/3)
    plt.show()
