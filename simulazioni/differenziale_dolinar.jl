#!/usr/bin/env julia

using LinearAlgebra;
import PyPlot;
import Optim;
import DelimitedFiles;
using Printf;
using PyCall;


# https://github.com/JuliaPy/PyCall.jl/issues/48
# https://stackoverflow.com/questions/52266203/unable-to-boot-juno
pushfirst!(PyVector(pyimport("sys")["path"]), "")
plot_database = pyimport("plot_database")

include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    error_probability, helstrom_success, log_approx_helstrom_error


function θ(P::Float64, n::Int64)
    if isapprox(P, .5)
        return π/4
    end
    return abs(.5*atan(sqrt(n)/(n - 1)*1. /(P - .5)))
end



function λ(α::Complex{Float64}, n::Int64, P::Float64)
    θ_ = θ(P, n)
    return abs(α)^2 * (cos(θ_) - sqrt(n)*sin(θ_))^2
end

function μ(α::Complex{Float64}, n::Int64, P::Float64)
    θ_ = θ(P, n)
    return abs(α)^2 * (cos(θ_) + sqrt(n)*sin(θ_))^2
end

function increment(α::Complex{Float64}, n::Int64, P::Float64)
    return μ(α, n, P) - (λ(α, n, P) + μ(α, n, P))*P
end


function helstrom(α::Complex{Float64})
    return .5*(1 + sqrt(1 - exp(-4*abs(α)^2)))
end

function simulate(α::Complex{Float64}, n::Int64, stop::Float64)
    size = 10000
    x = range(0., stop, length=size)
    y = fill(.5, size)
    hel = helstrom(α*sqrt(stop))
    z = range(hel, hel, length=size)
    inc = stop/size
    for i in 1:(size - 1)
        y[i + 1] = y[i] + inc*increment(α, n, y[i])
    end
    return x, y, z
end


function plot_for_n(α::Complex{Float64}, n::Int64, plot_helstrom::Bool = false)
    x, y, z = simulate(α, n, 2.)
    θvec = [θ(yy, n) for yy in y]
        PyPlot.subplot(2, 1, 1)
    if plot_helstrom
        PyPlot.plot(x, z, linestyle="--", marker="", color="red", label="Helstrom bound")
    end
    PyPlot.plot(x, y, linestyle="-", marker="", label="\$2n = $n\$")
    PyPlot.subplot(2, 1, 2)
    PyPlot.plot(x, θvec, linestyle="-", marker="" , label="\$2n = $n\$")
end

function bellurie(subplot)
    PyPlot.subplot(2, 1, subplot)
    PyPlot.minorticks_on()
    PyPlot.grid("gray")
    # PyPlot.ylim(0, 1)
    PyPlot.xlim(0, 2)
    PyPlot.legend()

end


function plot_evolution(α::Complex{Float64})
    PyPlot.figure(1, figsize=(10, 9))
    PyPlot.rc("font", size=16)
    plot_for_n(α, 10, true)
    for n in [20, 50, 100, 500]
        plot_for_n(α, n)
    end
    bellurie(1)
    PyPlot.title("Decision success probability \$P_c\$")
    PyPlot.xlabel("\$t\$", fontsize=14)
    PyPlot.ylabel("\$P_c\$", fontsize=14)
    bellurie(2)
    PyPlot.title("Control policy")
    PyPlot.ylabel("\$\\theta\$", fontsize=14)
    PyPlot.xlabel("\$t\$", fontsize=14)
    PyPlot.subplot(2, 1, 1)
    # PyPlot.yscale("log")
    all_parameters = Dict(
        "abs(alpha)" => abs(α),
    )
    plot_database.save_plot(
        title="Policy dolinar",
        file_path_base="policy_evolution_dolinar",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )
end


function logrange(x1::Float64, x2::Float64, n::Int64)
    return (10^y for y in range(log10(x1), log10(x2), length=n))
end


function n_dependence(α::Complex{Float64})
    size = 50
    values = logrange(2., 10000., size)
    values = [convert(Int64, round(x, digits=0)) for x in values]
    succ = fill(0., size)
    hel = 0.
    for i in 1:size
        x, y, z = simulate(α, values[i], 2.)
        hel = z
        succ[i] = last(y)
    end
    PyPlot.figure(2, figsize=(10, 9))
    PyPlot.rc("font", size=16)
    PyPlot.plot(values, succ, linestyle="-", color="tab:blue")
    helvalues = fill(hel, size)
    # @info "Cusumano" helvalues succ values
    PyPlot.plot(values, helvalues, linestyle="--", color="red")
    PyPlot.xlabel("\$2n\$", fontsize=14)
    PyPlot.ylabel("\$P_c\$", fontsize=14)
    # PyPlot.ylim(.5, 1)
    PyPlot.title("Correct decision probability")
    PyPlot.xscale("log")
    PyPlot.minorticks_on()
    PyPlot.grid("gray")
    # PyPlot.legend(loc="lower right")
    all_parameters = Dict(
        "abs(alpha)" => abs(α),
    )
    plot_database.save_plot(
        title="Success probability dolinar",
        file_path_base="success_dolinar",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )
end


function α_dependence(n::Int64)
    size = 5
    α_values = range(0.1, 5, length=size)
    PyPlot.figure(3, figsize=(10, 9))
    PyPlot.rc("font", size=16)
    PyPlot.xlabel("\$t\$", fontsize=14)
    PyPlot.ylabel("\$\\theta\$", fontsize=14)
    PyPlot.title("Optimal policy")
    PyPlot.minorticks_on()
    PyPlot.grid("gray")

    for i in 1:size
        α = α_values[i] + 0im
        x, y, z = simulate(α, n, 2.)
        θvec = [θ(yy, n) for yy in y]
        val = @sprintf("%.2f", abs(α))
        PyPlot.plot(x, θvec, label="\$|\\alpha| = $val\$")
    end
    PyPlot.legend()
    all_parameters = Dict(
        "n" => n,
    )
    plot_database.save_plot(
        title="Dolinar variazione con alpha",
        file_path_base="alpha_variation_dolinar",
        parameters=all_parameters,
        script_filename=@__FILE__,
    )

end

if abspath(PROGRAM_FILE) == @__FILE__
    # plot_evolution(0.25 + 0im)
    # n_dependence(0.25 + 0im)
    α_dependence(20)
    PyPlot.show()

end
