"""
Questo file include un sacco di funzioni utili da riutilizzare
per il quantum perceptron. Decisamente da scrivere in un posto solo per
evitare di correggere in posti diversi.
"""

module QuantumPerceptron

using LinearAlgebra: norm;
using Printf;
import SpecialFunctions


export helstrom_success, log_approx_helstrom_error, success_probability, therm_disp_nρn,
    factorial_app, binomial_app


function helstrom_success(α::Complex{Float64}, β::Complex{Float64})
    return .5*(1 + sqrt(1 - exp(-abs(α - β)^2)))
end


"""Errore helstrom approssimato quando gli stati sono lontani.
"""
function log_approx_helstrom_error(α::Complex{Float64}, β::Complex{Float64})
    if abs(α - β)^2 < 5
        @error "Richiesto helstrom prob approssimato con stati vicini. Sconsigliabile."
    end
    return -abs(α - β)^2 + log(.25)
end




"""
Restituisce un fattoriale come Float.
Sotto n = 20 è "esatto", oltre usa stirling.
Sono scandalizzato dal fatto che non sia già pronta una funzione simile.
"""
function factorial_app(n::Int64)::Float64
    if n < 20
        return Float64(factorial(n))
    else
        return sqrt(2π*n)*(n/exp(1))^n*(1 + 1/12n - 1/360n^3)
    end
end

function binomial_app(n, m)
    return factorial_app(n)/(factorial_app(m)*factorial_app(n - m))
end





"""Questa funzione restuisce la probabilità di misurare un certo numero di fotoni
su uno stato termico displaced.
Sostanzialmente ritorna \braket{n}{\rho}{n}.
Complessità O(n)
"""
function therm_disp_nρn(n::Int64, xnorm::Float64, σ::Float64)
    @debug "Chiesto thermal disp" n xnorm σ
    retval = 0.;
    x = xnorm^2/(1 + σ);
    if isapprox(σ, 1)
        retval = x^n/factorial_app(n)
    else
        schif1 = 2/(1 + σ);
        schif2 = (σ - 1)/(σ + 1);
        for m in 0:n
            retval += x^m/factorial_app(m)*binomial_app(n, m)*schif1^(m + 1)*schif2^(n - m)
        end
    end
    retval *= exp(-x)
    @debug "Probabilità photocounting" n xnorm σ x retval
    return retval
end




"""Questa funzione restituisce la probabilità che una misura
abbia successo.
Complessità O(n^3)
"""
function success_probability(x1::Float64, x2::Float64, σ::Float64)
    return perceptron_probability(x1, x2, σ, true)
end

function error_probability(x1::Float64, x2::Float64, σ::Float64)
    return perceptron_probability(x1, x2, σ, false)
end



function log_factorial_app(n::Int64)
    if n < 20
        return log(factorial(n))
    end
    return .5*log(2π*n) + n*log(n) - n + log(1. + 1/(12n) - 1/(360n^3))
end


function poissonian(μ::Float64, n::Int64)
    return exp(n*log(μ) - μ - log_factorial_app(n))
end


"""
Questa funzione calcola P(n_1 > n_2) + 0.5 P(n_1 = n_2)
"""
function coherent_probability(x1::Float64, x2::Float64, success::Bool)
    if isapprox(x1, 0.)
        return .5*exp(-x2)
    end
    if isapprox(x2, 0.)
        return 1 - 0.5*exp(-x1)
    end
    prob = 0;
    n1 = 0
    cutoff = 1e-12
    @debug "Starting conto coherent"

    while true
        partial = 0
        for n2 in 0:(n1 - 1)
            @debug "Loop interno" n1 n2 x1 x2 partial
            partial += poissonian(x2, n2)
            if partial > 1
                partial = 1
                @debug "Cutting off partial" 1
                break
            end
        end
        @debug "Somma interna" partial
        partial *= poissonian(x1, n1)
        @debug "Somma interna modificata" partial
        if partial < cutoff && n1 > 1.5*x1
            @debug "Droppo, è piccolo" partial n1 x1
            break
        end
        prob += partial
        n1 += 1
    end
    @debug "Parziale asimmetrico" prob n1

    prob += 0.5*equal_probability_coherent(x1, x2)
    return prob;
end



function equal_probability_coherent(x1::Float64, x2::Float64)
    prob = 0.
    n = 0
    cutoff = 1e-12
    while true
        partial = exp(n*log(x1*x2) - x1 - x2)/(factorial_app(n))^2
        if partial < cutoff && n > (x1 + x2)
            @debug "Cutoff 1/2" x1 x2 n
            break
        end
        prob += partial
        n += 1
    end
    return prob
end


function gaussian_probability(x1::Float64, x2::Float64, σ::Float64, success::Bool)
    prob = 0.;
    cutoff = 1e-12
    cutoff2 = 1e-4
    n1 = 0
    compute = true
    while compute
        p1 = therm_disp_nρn(n1, x1, σ)
        if success
            for n2 in 0:n1
                prob += therm_disp_nρn(n2, x2, σ)*p1;
            end
        else
            n2 = n1 + 1
            while true
                p2 = therm_disp_nρn(n2, x2, σ)
                @debug "Risssschio" n2 x2 σ success p2
                prob += p2*p1;
                n2 += 1
                if p2 < cutoff2 && (n2 >= 2*x2)
                    @debug "Finito ciclo su p2" n1 x1 n2 x2 σ success p1 p2
                    break
                end
            end
        end
        if p1 < cutoff && (n1 >= 2*x1)
            @debug "Cutting off" n1 x1 x2 p1 σ prob
            break
        end
        n1 += 1
    end

    @debug "Probabilità di successo" prob
    if prob > 1
        if prob > 1.1
            @error "Probabilità di successo molto maggiore di 1" prob x1 x2 σ
        elseif prob > 1.0001
            @warn "Probabilità di successo maggiore di 1" prob x1 x2 σ
        else
            prob = 1.
        end
    end
    if prob < .5
        @debug "Probabilità di successo minore di 1/2" prob x1 x2 σ
    end
    if prob < 0.
        @error "Probabilità di successo minore di 0" prob x1 x2 σ
    end
    return prob;
end


function perceptron_probability(x1::Float64, x2::Float64, σ::Float64, success::Bool)
    @debug "Chiesta perceptron prob con " x1 x2 σ success
    if x1 < 0 || x2 < 0
        throw(DomainError((x1, x2), "x1 e x2 devono essere entrambe positivi"))
    end
    if isapprox(σ, 1)
        return coherent_probability(x1, x2, success)
    else
        return gaussian_probability(x1, x2, σ, success)
    end
end




end
