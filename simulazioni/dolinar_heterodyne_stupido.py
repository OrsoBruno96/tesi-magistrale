#!/usr/bin/env python3
"""Stavolta facciamo misure heterodyne
"""
from matplotlib import pyplot as plt
from numpy import exp, array as nparr, sqrt
from numpy.random import normal, seed
from logging import getLogger, Formatter, StreamHandler, INFO, DEBUG
from math import pi
from argparse import ArgumentParser

logger = getLogger(__file__)
beta = 10  # $\frac{\hbar \omega}{k\ped{B} T}$
center = (5, 0)
caso = "A"  # a scelta fra "A" e "B"
maxdiv = 31
iterations = 50


def plot(A, B, fignum, sdevA=None, sdevB=None):
    plt.figure(fignum)
    plt.title(f"Posteriori caso {caso}")
    plt.errorbar(
        range(0, len(A)), A, sdevA,
        label='Posteriore per A', marker='x')#, color='tab:blue')
    # plt.errorbar(
    #     range(0, len(B)), B, sdevB,
    #     label='Posteriore per B', marker='x')#, color='tab:orange')
    # plt.yscale('log')
    plt.xlabel("Iterazione")
    plt.ylabel("Posteriore")
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()


def prob(outcome, center):
    """$\langle \alpha | \rho_G | \alpha \rangle
    """
    dist2 = (outcome[0] - center[0])**2 + \
        (outcome[1] - center[1])**2
    return (1-exp(-beta))*exp(-(1-exp(-beta))*dist2)


def extract_center(center: tuple) -> tuple:
    sigma = sqrt(2/(1-exp(-beta)))
    logger.debug(f"Posizione del centro: \033[0;31m{center}\033[0m, sigma: {sigma}")
    return (normal(center[0], sigma), normal(center[1], sigma))


def get_center(swapped: bool, division) -> tuple:
    if swapped:
        return (center[0]/sqrt(division), center[1]/division)
    return (0, 0)


def new_priors(old_priors: tuple, outcome: tuple, swapped) -> tuple:
    if not swapped:
        centers = ((0, 0), center)
    else:
        centers = (center, (0, 0))
    p1 = prob(outcome, centers[0])
    p2 = prob(outcome, centers[1])
    return (
        p1*old_priors[0]/(p1*old_priors[0] + p2*old_priors[1]),
        p2*old_priors[1]/(p1*old_priors[0] + p2*old_priors[1]),
    )


def simula(fignum, division, do_plot=False):
    swapped = False
    # Inizializzo a dimensione fissa così uso numpy che è più fast
    priors = nparr([
        (.5, .5) for p in range(0, division + 1)
    ])
    for i in range(0, division):
        """In base a se sono swappati o meno capisco che distribuzione di prob ho.
        In funzione di questa estraggo con una certa prob la misura.
        Calcolo i posteriori
        Faccio l'update dei priori e decido se scambiare o meno il cusumano
        """
        center = get_center((caso == "B") ^ swapped, division)
        outcome = extract_center(center)
        logger.info(f"Estratto nuovo centro: {outcome}")
        priors[i + 1] = new_priors(
            old_priors=priors[i], outcome=outcome, swapped=swapped)
        swapped = priors[i + 1][0] < priors[i + 1][1]
    A, B = zip(*priors)
    if do_plot:
        plot(A, B, fignum=fignum)
    return nparr(A), nparr(B)


def start_logger():
    logger.setLevel(DEBUG)
    formatter = Formatter('%(asctime)s - %(name)s - %(levelname)s \n%(message)s')
    sh = StreamHandler()
    sh.setLevel(DEBUG)
    sh.setFormatter(formatter)
    logger.addHandler(sh)


def repeat_simulations():
    for i, division in enumerate(range(1, maxdiv, 10)):
        dd = division + 1
        avgsA, sdevsA = nparr([0.]*dd), nparr([0.]*dd)
        avgsB, sdevsB = nparr([0.]*dd), nparr([0.]*dd)
        for rep in range(0, iterations):
            A, B = simula(i, division, False)
            avgsA += A
            sdevsA += A**2
            avgsB += B
            sdevsB += B**2
        avgsA /= iterations
        sdevsA /= iterations
        avgsB /= iterations
        sdevsB /= iterations
        sdevsA = sqrt(sdevsA - avgsA**2)
        sdevsB = sqrt(sdevsB - avgsB**2)
        print(sdevsA)
        plot(A=avgsA, B=avgsB, fignum=1, sdevA=sdevsA, sdevB=sdevsB)


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("--save", help="Vuoi salvare il plot?")
    parser.add_argument(
        "--output", help="Su che file vuoi salvare il risultato?",
        default="output/dolinar_heterodyne_stupido.pdf")
    args = vars(parser.parse_args())
    start_logger()
    seed(1234)
    repeat_simulations()
    plt.show()
