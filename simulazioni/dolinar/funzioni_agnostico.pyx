from libc.math cimport exp, pow, M_PI, sqrt as csqrt
from libc.complex cimport cabs
from cython import cdivision, boundscheck, wraparound
from logging import getLogger
from numpy import array as nparr, sqrt, zeros
from numpy.random import normal
cimport numpy as np
import numpy as np


logger = getLogger("funzioni_agnostico.py")

cdef double pi = M_PI
ctypedef np.complex_t COMPLEX_TYPE_t
ctypedef np.double_t REAL_TYPE_t


@cdivision(True)
cdef double _gaus(double complex var, double complex center, double sigma):
    return 1./(2*pi)*exp(-pow(cabs(center - var), 2)/(2*sigma))


@cdivision(True)
cdef double _newsigma(double sigma1, double sigma2):
    return 1./(1./sigma1 + 1./sigma2)


@cdivision(True)
cdef double complex _newcenter(double complex c1, double complex c2,
     double sigma1, double sigma2, double newsigma):
    return (c1/sigma1 + c2/sigma2)*newsigma


@cdivision(True)
cdef double _newexponent(double complex c1, double complex c2,
     double sigma1, double sigma2, double newsigma):
    return .5*(pow(cabs(c1/sigma1 + c2/sigma2), 2)*newsigma - \
           (pow(cabs(c1), 2)/sigma1 + pow(cabs(c2), 2)/sigma2))


@cdivision(True)
cdef double _newnorm(double complex c1, double complex c2, double sigma1,
     double sigma2, double newsigma):
    return exp(_newexponent(c1, c2, sigma1, sigma2, newsigma))/(2*pi*(sigma1 + sigma2))


@cdivision(True)
cdef compute_posteriors(double complex misura,
    np.ndarray[COMPLEX_TYPE_t, ndim=2] oldpriors, bint swapped):
    """Questa funzione è il cancro più assoluto.
    """
    cdef double newsigma
    cdef double complex newcenter
    cdef double stupido
    cdef double altranorm
    cdef double anorm
    cdef double aanorm
    if not swapped:
        newsigma = _newsigma(1, oldpriors[1][2])
        newcenter = _newcenter(misura, oldpriors[1][1], 1, oldpriors[1][2], newsigma)
        stupido = oldpriors[0][0]*_gaus(misura, 0, 1)
        altranorm = oldpriors[1][0]*_newnorm(
            misura, oldpriors[1][1], 1, oldpriors[1][2], newsigma)
        anorm = stupido/(stupido + altranorm)
        aanorm = altranorm/(stupido + altranorm)
        retval = nparr([
            [anorm, oldpriors[0][1], oldpriors[0][2]],
            [aanorm, newcenter, newsigma],
        ])
    else:
        newsigma = _newsigma(oldpriors[0][2], 1)
        newcenter = _newcenter(oldpriors[0][1], misura, oldpriors[0][2], 1, newsigma)
        stupido = oldpriors[1][0]*_gaus(misura, 0, 1)
        altranorm = oldpriors[0][0]*_newnorm(
            oldpriors[0][1], misura, oldpriors[0][2], 1, newsigma)
        anorm = stupido/(stupido + altranorm)
        aanorm = altranorm/(stupido + altranorm)
        retval = nparr([
            [aanorm, newcenter, newsigma],
            [anorm, oldpriors[1][1], oldpriors[1][2]],
        ])
    # logger.debug(f"newsigma: {newsigma}")
    # logger.debug(f"newcenter: {newcenter}")
    # logger.debug(f"Stupido: {stupido}, altranorm: {altranorm}")
    return retval


cdef initial_priors(double sigma0):
    """I priori sono joint priors p(d, A) e p(d, B), per cui
    sono tutte e due PDF. Avere a disposizione funzioni complica
    parecchio il tutto ma dato che se partiamo con gaussiane ci rimangono
    gaussiane, vediamo ci bastano 3 variabili per ognuna. Sono 3 e non 2
    perché il fatto di avere due possibilità, A e B, ci aggiunge una
    ulteriore normalizzazione davanti.
    """
    return nparr([
        [.5, 0., sigma0],
        [.5, 0., sigma0],
    ])


@cdivision(True)
cdef double complex get_center(bint centrato, int divisions, double reald):
    """Restituisci la posizione del centro dello stato coerente
    alla fine del circuito.
    """
    if centrato:
        return 0
    return reald/csqrt(divisions)


cdef double complex extract_center(double complex center, double sigma2=1):
    """Estrae il risultato di una misura heterodyne sapendo che lo stato
    originale ha centro nel posto opportuno.
    """
    return normal(center.real, sigma2) + 1j*normal(center.imag, sigma2)


@cdivision(True)
@boundscheck(False)
@wraparound(False)
def repeat_simulations(int maxdiv, int spacing, int repetitions,
                           double sigma0, bint caso, double reald):
    """Questa funzione andrà modificata per ripetere la simulazione tante
    volte e per farla per diversi valori di 'divisions'
    """
    cdef np.ndarray[REAL_TYPE_t, ndim=1] endprob = zeros([maxdiv // spacing + 1], dtype=np.double)
    cdef np.ndarray[REAL_TYPE_t, ndim=1] endsigma = zeros([maxdiv // spacing + 1], dtype=np.double)
    cdef np.ndarray[REAL_TYPE_t, ndim=1] A, B, sA, sB
    cdef int _, i
    cdef bint swapped, centrato
    cdef double complex misura
    cdef np.ndarray[COMPLEX_TYPE_t, ndim=3] priors
    for divisions in range(0, maxdiv, spacing):
        logger.info(f"Starting with {divisions} divisions")
        A = zeros([divisions], dtype=np.double)
        B = zeros([divisions], dtype=np.double)
        sA = zeros([divisions], dtype=np.double)
        sB = zeros([divisions], dtype=np.double)
        for _ in range(repetitions):
            if (_ % 100) == 0:
                logger.info(f"Divisions: {divisions}, repeat: {_}")
            swapped = False
            priors = nparr(zeros([divisions + 1, 2, 3], dtype=np.complex_))
            priors[0] = initial_priors(sigma0)
            for i in range(divisions):
                centrato = caso ^ swapped
                logger.debug(f"Centrato: {centrato}")
                misura = extract_center(get_center(centrato, divisions, reald))
                logger.debug(f"Misura: {misura}\npriors: {priors[i]}\nswapped: {swapped}")
                logger.debug(f"New posteriors: {compute_posteriors(misura, priors[i], swapped)}")
                priors[i + 1] = compute_posteriors(misura, priors[i], swapped)
                swapped = priors[i + 1, 0, 0].real < .5
                logger.debug(f"Swapped: {swapped}")
                A[i] += priors[i + 1, 0, 0].real
                B[i] += priors[i + 1, 1, 0].real
                sA[i] += pow(priors[i + 1, 0, 0].real, 2)
                sB[i] += pow(priors[i + 1, 1, 0].real, 2)
        A /= repetitions
        B /= repetitions
        sA = sqrt(sA/repetitions - A**2)
        sB = sqrt(sB/repetitions - B**2)
        # plot_priors(priors, fignum=divisions)
        if not A.any():
            continue
        # TODO: sqlite per database grafici
        # plot(A, B, fignum=1, sdevA=sA, sdevB=sB, logy=False,
        #      savename=f"caso_{caso}_d_{reald}_verboso.pdf")
        # plot(A, B, fignum=2, sdevA=sA, sdevB=sB, logy=True,
        #      savename=f"caso_{caso}_d_{reald}_verboso_log.pdf")
        endprob[int(divisions / spacing)] = A[-1]
        endsigma[int(divisions / spacing)] = sA[-1]
    return spacing, endprob, endsigma
