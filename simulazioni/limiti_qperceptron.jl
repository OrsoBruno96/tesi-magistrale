#!/usr/bin/env julia

using LinearAlgebra;
import PyPlot;
using Printf;
using PyCall;


# https://github.com/JuliaPy/PyCall.jl/issues/48
# https://stackoverflow.com/questions/52266203/unable-to-boot-juno
pushfirst!(PyVector(pyimport("sys")["path"]), "")
plot_database = pyimport("plot_database")

include("QuantumPerceptron.jl");
using .QuantumPerceptron: therm_disp_nρn, success_probability,
    helstrom_success, error_probability


# Dimensione di R^n
const n = 3;
# Totale di punti di training generati (e in questo caso anche di passi dell'update)
const N = 500;

# Le seguenti variabili sono definite sul logbook e indicano quanto pezzo viene
# usato per il training e quanto conservato
const θ1 = 0.1;
const θ2 = 0.1;
const α = 0.1;
const data1 = -3.0;
const data2 = -data1;
const σ = 1.;
const initial = 0.; # Trucco
const size = 20


"""
Funzione che restituisce le componenti mischiate con il BS gate al 50%
e la sigma corrispondente.
"""
function get_mixed_components(data::Float64, weights::Float64, n::Int64)
    x1 = (sin(θ1)*data + sin(θ2)*weights)/sqrt(2.)
    x2 = (sin(θ1)*data - sin(θ2)*weights)/sqrt(2.)
    x1 = x1^2
    x2 = x2^2
    σk = get_σ_k(n)
    σa = (sin(θ1)^2 *σ + cos(θ1)^2 + cos(θ2)^2 * σk + sin(θ2)^2)/2
    return x1, x2, σa
end


function get_σ_k(k::Int64)
    if isapprox(σ, 1)
        return 1.
    end
    a = cos(α)^2*cos(θ2)^2
    b = sin(α)^2*(cos(θ1)^2*σ + sin(θ1)^2) + cos(α)^2*sin(θ2)^2;
    return a^k + b*(1 - a^(k + 1))/(1 - a)
end



function valutazione(ran_d, ran_w)
    size = length(ran_w)
    PyPlot.figure(1)
    color_cycle = PyPlot.matplotlib.rcParams["axes.prop_cycle"].by_key()["color"]
    for (j, d) in enumerate(ran_d)
        pesi = ran_w
        probs = Array{Float64, 1}(undef, size)
        @info "Stiamo facendo d = $d. Ci si ferma a 50"
        @time for i in 1:size
            x1, x2, σth = get_mixed_components(d, pesi[i], i)
            probs[i] = error_probability(x1, x2, σth)
        end
        idx = j % length(color_cycle)
        idx = idx != 0 ? idx : length(color_cycle)
        color = color_cycle[idx]
        PyPlot.plot(
            ran_w, probs, linestyle="-", marker="",
            label=@sprintf("\$d\$ = %.2f", d),
            color=color,
        )
        hel_err = 1 - helstrom_success(complex(d), complex(-d))
        hel = fill(hel_err, size)
        PyPlot.plot(
            ran_w, hel, linestyle="--", marker="", color=color
        )
    end
    PyPlot.title("Probabilità errore lungo la curva")
    PyPlot.grid("gray")
    PyPlot.xlabel("Iterazione")
    PyPlot.ylabel("Probabilità errore")
    PyPlot.yscale("log")
    PyPlot.minorticks_on()
    PyPlot.legend()
    PyPlot.savefig("julia-plots/valutazione_funzione.pdf")
end


function pos_piano(ran_d, ran_w)
    PyPlot.figure(2)
    size = length(ran_w)
    for d in ran_d
        x = Array{Float64, 1}(undef, size)
        y = Array{Float64, 1}(undef, size)
        for i in 1:size
            x[i], y[i], σ = get_mixed_components(d, ran_w[i], i)
        end
        PyPlot.plot(
            x, y, linestyle="-", marker="",
            label=@sprintf("\$d\$ = %.2f", d),
        )
    end
    PyPlot.title("Posizione sul piano valutazione")
    PyPlot.grid("gray")
    PyPlot.minorticks_on()
    PyPlot.legend()
    PyPlot.savefig("julia-plots/posizione_piano.pdf")
end


if abspath(PROGRAM_FILE) == @__FILE__
    ran_d = range(0.1, stop=5, length=size)
    ran_w = range(0, stop=50, length=size*5)
    valutazione(ran_d, ran_w)
    pos_piano(ran_d, ran_w)

    PyPlot.show()
end
