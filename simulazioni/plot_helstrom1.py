#!/usr/bin/env python3


from matplotlib import pyplot as plt
from numpy import exp, array as nparr, sqrt, linspace


if __name__ == "__main__":
    plt.figure(1, figsize=(10, 9))
    x = linspace(0, 6, 1000)
    helstrom = 0.5*(1 - sqrt(1 - exp(-x)))
    nostro = 0.5*exp(-x)
    plt.plot(x, nostro, color='blue', label='Optical discriminator')
    plt.plot(x, helstrom, color='red', label='Helstrom bound')
    # plt.plot(x, nostro/2, color='blue', label='Nostro/2', linestyle='--')

    plt.rc('font', size=16)
    plt.title("Oneshot approach")
    plt.xlabel("$|d|^2$", fontsize=14)
    plt.ylabel("Error probability", fontsize=14)
    plt.yscale('log')
    plt.grid('gray')
    plt.minorticks_on()
    plt.legend()
    plt.savefig("plots/dolinar_oneshot.pdf")

    plt.show()
