#!/usr/bin/env python3
from matplotlib import pyplot as plt
from numpy import exp, linspace, sqrt
from scipy.stats import poisson
from scipy.special import iv as BesselI
from scipy import integrate
from scipy.optimize import bisect
import numpy as np


def prob(theta, d0):
    return .5*exp(-.5*(d0**2 + theta))*BesselI(0, d0*sqrt(theta))


def integration(stop, param):
    return integrate.quad(lambda x: prob(x, param), 0., stop)[0]


def test_normalization():
    for param in linspace(.1, 6., 10):
        res = integrate.quad(lambda x: prob(x, param), 0., 100.)
        if res[0] < 0.99 or res[0] > 1.01:
            raise ValueError(f"Integral test evaluation is not normalized for d_0 = {param}!")


def helstrom_success(dsq):
    return .5*(1 - sqrt(1 - exp(-0.5*dsq)))


def invert_function(x, y):
    retx = list(linspace(min(y), max(y), len(y)))
    rety = []
    for yy in retx:
        idx = find_nearest(y, yy)
        rety.append(x[idx])
    return retx, rety


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


def neyman_construction():
    stop = 10
    d0_list = linspace(0, stop, 100)
    theta_list = []
    for d0 in d0_list:
        result = bisect(lambda x: integration(x, d0) - CL, 0, 200)
        theta_list.append(result)
    # theta_list = sqrt(theta_list)
    plt.plot()

    plt.figure(1, figsize=(10, 9))
    plt.plot(d0_list, theta_list, linestyle='')
    plt.fill_between(d0_list, 0, theta_list, hatch='/', edgecolor='tab:blue', alpha=0.3)
    plt.xlim(0, stop)
    plt.ylim(0, max(theta_list))
    plt.rc('font', size=16)
    plt.title("Neyman construction $CL = 0.95$")
    plt.xlabel(r"$|d_0|^2$", fontsize=14)
    plt.ylabel(r"$\theta$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.savefig("plots/neyman_construction_heterodyne.pdf")
    plt.figure(2, figsize=(10, 9))
    meas, dsq = invert_function(d0_list, theta_list)
    prob = [helstrom_success(d) for d in dsq]
    plt.plot(meas, prob, linestyle='', marker='o')
    plt.xlim(0, max(meas))
    plt.ylim(0, .5)
    plt.rc('font', size=16)
    plt.title("Estimated error probability")
    plt.xlabel(r"$\theta$", fontsize=14)
    plt.ylabel(r"$P_{\mathrm{err}}$", fontsize=14)
    plt.grid('gray')
    plt.minorticks_on()
    plt.savefig("plots/neyman_error_prob_heterodyne.pdf")


if __name__ == "__main__":
    CL = 0.95
    test_normalization()
    neyman_construction()
    plt.show()
