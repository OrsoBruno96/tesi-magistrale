        defbox  supermixer,3,0,'U'
        defbox  bs50,2,0,'BS_{50}'
        defbox  pi,1,0,'\pi'
        defbox  bseps,2,0,'BS_{\frac{1}{\sqrt{2N}}'

        qubit   \alpha_n
        qubit   \alpha_2
        qubit   \alpha_1
        qubit   \beta_1
        qubit   \beta_2
        qubit   \beta_n
        qubit   \delta

        supermixer       \alpha_1,\alpha_2,\alpha_n
        supermixer       \beta_1,\beta_2,\beta_n
        bs50             \alpha_1,\beta_1
        pi               \alpha_1
        discard          \alpha_2
        discard          \alpha_n
        discard          \beta_2
        discard          \beta_n
#       bseps            \alpha_1,\delta
#       bseps            \beta_1,\delta
#       dmeter           delta